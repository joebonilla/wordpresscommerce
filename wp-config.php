<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'iwaNnaknoWwhatLove1$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E[-/^.%kklQ7Bb8hOFpVt61Q=HqXym-x,WYH.bHPWX[`,ZDaP4(w2jj;oY?9zoE>');
define('SECURE_AUTH_KEY',  'kWq_xT!v%au|@fKM{wm9f+:QGmZR=(3aQ__:qJJ*HVL:BJoK??amfG9}.~Gv!-4$');
define('LOGGED_IN_KEY',    'KY,]fYOp(^Alaf^)?8zp;:`G|-<=-]UW<>xnuwAF{m/{tk&b|sgz%xav58y|4/w2');
define('NONCE_KEY',        'i*ae!p=I$/&:rz>|AUU4FGBemp62+/J]i;EC{3<S[SH_j$YSv/!6]H:$5fYqWrC-');
define('AUTH_SALT',        '+Lsi:uR@>)RY.%(!-9Y%jCE@]h1goK`R+jNxgP^*vO#e-LeyD1eG4O{W&*Qotdxw');
define('SECURE_AUTH_SALT', 'OMg:h|Qx?G^+KGGwPHXdqm, FvJY%d2:pspFW;5)_ODuo4PFLzuuO[Gi)[aHRquR');
define('LOGGED_IN_SALT',   '>3yZY7QPoZWB&T0bj7;Ttio! cifbs`VjU48bTqLVPh]$}u4G5),Ui)+_W`l}Q-1');
define('NONCE_SALT',       'RBk[RJ2HZNf0o*VZ0--``sw`VX$/g>q=<Bgv[_8P`r>F2~KL?`P6KSF]_u~U@vr.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wordpress_test_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
