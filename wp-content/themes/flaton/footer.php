<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package flaton
 */
?>

</div><!-- #content -->
</div>

<footer id="colophon" class="site-footer" role="contentinfo">
    <?php
    global $flaton;
    if ($flaton['footer-widgets']) : ?>
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <?php get_template_part('footer', 'widgets'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="footer-bottom">
        <div class="container">
            <div class="sixteen columns">
                <div class="site-info">
                    <?php do_action('flaton_credits'); ?>
                </div><!-- .site-info -->
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
    window.onload = function () {
        window.addEventListener('message', function (event) {
            console.log(event.data);
            if (event.origin != 'http://dev-project.wideout.com/iframeanalytics/') return;
            ga('create', 'UA-75356567-1', 'auto', {
                allowLinker: true,
                clientId: event.data
            });
            ga('linker:autoLink', ['dev-project.wideout.com']);
            ga('require', 'linker');
        });
    };
</script>
</body>
</html>
